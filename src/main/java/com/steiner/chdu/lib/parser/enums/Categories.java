package com.steiner.chdu.lib.parser.enums;

/**
 * Created by: Ihor Kushnirenko <SteinerOk@gmail.com>
 * Date: 20.12.2015 1:23
 */

/**
 * Список категорий, доступных для парсинга обычным парсером.
 */
public enum Categories {
    /**
     * Підручники
     */
    TUTORIALS(1),
    /**
     * Посібники
     */
    MANUALS(2),
    /**
     * Методична серія
     */
    METHODICS(3),
    /**
     * Авторська лекція
     */
    AUTHORS(4),
    /**
     * Для самостійного вивчення
     */
    INDSTUDY(5),
    /**
     * Курс лекцій
     */
    LECTIONS(28),
    /**
     * Практикум
     */
    PRACTICS(29),
    /**
     * Доуніверситетська підготовка
     */
    PREUNIVERSITY(6),
    /**
     * Бібліографічний покажчик
     */
    LIBR(7),
    /**
     * Довідники
     */
    CATALOGS(21),
    /**
     * Монографії
     */
    MONOGRAPHYS(9),
    /**
     * Новітня філологія
     */
    MODERNPHILOLOGY(11),
    /**
     * Сучасна Укр. політика
     */
    MODERNUAPOLITICS(12),
    /**
     * Чорноморський літопис
     */
    BSCHRONICLES(25),
    /**
     * Історичний архів
     */
    HSTRYARCHIVE(26),
    /**
     * Україна: історія і сучасність
     */
    UAHSTRYMDRNTY(32),
    /**
     * Археологія
     */
    ARCHEOLOGY(27),
    /**
     * Автореферати
     */
    ABSTRACTS(14),
    /**
     * Дисертації
     */
    THESIS(15),
    /**
     * Укр. право
     */
    UALAW(31),
    /**
     * Студентські наукові студії
     */
    STUDENTSCST(13),
    /**
     * Збірники
     */
    COLLECTIONS(23),
    /**
     * Тези доповідей
     */
    THESES(24),
    /**
     * Галерея мистецтв
     */
    GALLERY(17),
    /**
     * Художні твори
     */
    LITERARYWORKS(18),
    /**
     * Ювілейні видання
     */
    ANNIVERSARY(22),
    /**
     * Історія голодомору
     */
    FAMINEHISTORY(20);

    private final int id;

    Categories(int id) {
        this.id = id;
    }

    /**
     * Метод получения id запрашиваемой категории для парсинга
     *
     * @return id категории для добавления в url
     */
    public int getId() {
        return id;
    }
}
