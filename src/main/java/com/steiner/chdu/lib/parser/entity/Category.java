package com.steiner.chdu.lib.parser.entity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by: Ihor Kushnirenko <SteinerOk@gmail.com>
 * Date: 20.12.2015 19:42
 */

public class Category {
    private String name;
    private String link;
    private Map<String, List<BookShort>> catalog;

    public Category() {
        catalog = new HashMap<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Map<String, List<BookShort>> getCatalog() {
        return catalog;
    }

    public void setCatalog(Map<String, List<BookShort>> catalog) {
        this.catalog = catalog;
    }

}
