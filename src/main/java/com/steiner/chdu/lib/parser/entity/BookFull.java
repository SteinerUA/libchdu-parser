package com.steiner.chdu.lib.parser.entity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by: Ihor Kushnirenko <SteinerOk@gmail.com>
 * Date: 20.12.2015 19:48
 */

public class BookFull {
    private String title;
    private String category;
    private String description;
    private String link;
    private Map<String, List<BookFullFile>> filesList;

    public BookFull() {
        filesList = new HashMap<>();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Map<String, List<BookFullFile>> getFilesList() {
        return filesList;
    }

    public void setFilesList(Map<String, List<BookFullFile>> filesList) {
        this.filesList = filesList;
    }

}
