package com.steiner.chdu.lib.parser.enums;

/**
 * Created by: Ihor Kushnirenko <SteinerOk@gmail.com>
 * Date: 20.12.2015 23:34
 */

/**
 * Список категорий раздела "Научные публикации", доступных для парсинга.
 */
public enum ScCategories {
    /**
     * Історія
     */
    HISTORY(1),
    /**
     * Техніка
     */
    MACHINERY(2),
    /**
     * Екологія
     */
    ECOLOGY(3),
    /**
     * Філологія. Літературознавство
     */
    LITERARY_CRITICISM(4),
    /**
     * Економіка
     */
    ECONOMY(5),
    /**
     * Філологія. Мовознавство
     */
    LINGUISTICS(10),
    /**
     * Педагогіка
     */
    PEDAGOGY(6),
    /**
     * Політичні науки
     */
    POLITICAL_SCIENCE(7),
    /**
     * Техногенна безпека
     */
    TECHNO_SECURITY(8),
    /**
     * Комп'ютерні технології
     */
    COMPUTER_TECHNOLOGY(9),
    /**
     * Соціологія
     */
    SOCIOLOGY(11),
    /**
     * Державне управління
     */
    GOVERNANCE(12),
    /**
     * Правознавство
     */
    SCIENCE_OF_LAW(13);

    private final int id;

    ScCategories(int id) {
        this.id = id;
    }

    /**
     * Метод получения id запрашиваемой категории раздела "Научные публикации" для парсинга
     *
     * @return id категории для добавления в url
     */
    public int getId() {
        return id;
    }
}
