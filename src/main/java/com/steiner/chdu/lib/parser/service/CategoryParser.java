package com.steiner.chdu.lib.parser.service;

import com.steiner.chdu.lib.parser.entity.BookShort;
import com.steiner.chdu.lib.parser.entity.Category;
import com.steiner.chdu.lib.parser.enums.Categories;
import com.steiner.chdu.lib.parser.utils.Constants;
import com.steiner.chdu.lib.parser.utils.HttpUtils;
import okhttp3.OkHttpClient;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by: Ihor Kushnirenko <SteinerOk@gmail.com>
 * Date: 20.12.2015 19:52
 */

public class CategoryParser {
    private Categories selectedCategory;
    private OkHttpClient httpClient;

    public CategoryParser(Categories selectedCategory) {
        this.selectedCategory = selectedCategory;
        this.httpClient = new OkHttpClient();
    }

    public CategoryParser(Categories selectedCategory, OkHttpClient httpClient) {
        this.selectedCategory = selectedCategory;
        this.httpClient = httpClient;
    }

    public Category parse() {
        String url = Constants.LIB_CAT_URL + selectedCategory.getId();
        String response = HttpUtils.makeGetRequest(url, httpClient);
        if (response.isEmpty())
            return null;

        Document document = Jsoup.parse(response);

        Category category = new Category();
        category.setLink(url);
        category.setName(getCatName(document));
        category.setCatalog(getCatalog(document));
        return category;
    }

    private boolean checkYearsAndBooksCount(Document doc) throws NullPointerException {
        Elements years = doc.select("div#main-content.main-content > font");
        Elements yearsContent = doc.body().select("div#main-content.main-content > table > tbody");

        if (years.size() != yearsContent.size())
            throw new NullPointerException("Несовпадает колличество годов и разделов книг");

        return true;
    }

    private String getCatName(Document doc) {
        String name = doc.select("div#main-content.main-content > div.text_head").text().trim();
        return (!name.isEmpty() ? name : "N/A");
    }

    private Map<String, List<BookShort>> getCatalog(Document doc) {
        Map<String, List<BookShort>> catalog = new HashMap<>();
        List<BookShort> booksList = new ArrayList<>();
        String year = "";

        doc = doc.clone();
        doc.select("div#main-content.main-content > div.text_head").remove();
        doc = Jsoup.parse(doc.select("div#main-content.main-content").html().replaceAll("<br>", ""));

        Elements elements = doc.body().children();
        for (Element element : elements) {
            String tYear = element.select("font").text().trim();
            if (!tYear.isEmpty()) {
                if (!year.isEmpty() && !booksList.isEmpty()) {
                    catalog.put(year, booksList);
                    booksList = new ArrayList<>();
                }
                year = tYear;
                continue;
            }

            Elements tBooks = element.select("table > tbody > tr");
            if ((tBooks.size() % 2) != 0)
                continue;

            int bookIndex = 0;
            while (bookIndex < tBooks.size()) {
                BookShort book = new BookShort();
                book.setAuthors(getBookAuthors(tBooks.get(bookIndex)));
                book.setImgLink(getBookImgLink(tBooks.get(bookIndex)));
                bookIndex++;
                book.setTitle(getBookTitle(tBooks.get(bookIndex)));
                book.setLink(getBookLink(tBooks.get(bookIndex)));
                bookIndex++;
                booksList.add(book);
            }
        }
        if (!year.isEmpty() && !booksList.isEmpty()) {
            catalog.put(year, booksList);
        }

        return catalog;
    }

    private String getBookTitle(Element element) {
        String title = element.text().trim();
        return (!title.isEmpty() ? title : "N/A");
    }

    private String getBookImgLink(Element element) {
        String imgLink = element.select("td > a > img[src]").attr("src").trim();
        return (!imgLink.isEmpty() ? Constants.LIB_URL + imgLink : "N/A");
    }

    private String getBookLink(Element element) {
        String link = element.select("td > a[href]").attr("href").trim();
        return (!link.isEmpty() ? Constants.LIB_URL + link : "N/A");
    }

    private String[] getBookAuthors(Element element) {
        String s = element.text().trim();
        if (s.isEmpty())
            return new String[0];

        String[] authors = s.split(",");
        for (int i = 0; i < authors.length; i++) {
            authors[i] = authors[i].trim();
        }
        return authors;
    }

}
