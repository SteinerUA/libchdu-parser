package com.steiner.chdu.lib.parser.service;

import com.steiner.chdu.lib.parser.entity.BookFull;
import com.steiner.chdu.lib.parser.entity.BookFullFile;
import com.steiner.chdu.lib.parser.utils.Constants;
import com.steiner.chdu.lib.parser.utils.HttpUtils;
import okhttp3.OkHttpClient;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.safety.Whitelist;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by: Ihor Kushnirenko <SteinerOk@gmail.com>
 * Date: 21.12.2015 2:14
 */

public class BookParser {
    private String url;
    private OkHttpClient httpClient;

    public BookParser(String url) {
        this.url = url;
        this.httpClient = new OkHttpClient();
    }

    public BookParser(String url, OkHttpClient httpClient) {
        this.url = url;
        this.httpClient = httpClient;
    }

    public BookFull parse() {
        String response = HttpUtils.makeGetRequest(url, httpClient);
        if (response.isEmpty())
            return null;

        Document document = Jsoup.parse(response);

        BookFull book = new BookFull();
        book.setLink(url);
        book.setTitle(getBookName(document));
        book.setCategory(getBookCategory(document));
        book.setDescription(getBookDescription(document.clone()));
        book.setFilesList(getBookFiles(document));
        return book;
    }

    private String getBookName(Document doc) {
        doc = doc.clone();
        doc.body().select("div#main-content.main-content > div.text_head > a").remove();
        String name = doc.body().select("div#main-content.main-content > div.text_head").text().replace("- ", "");
        return (!name.isEmpty() ? name : "N/A");
    }

    private String getBookCategory(Document doc) {
        String title = doc.body().select("div#main-content.main-content > div.text_head > a").text().trim();
        return (!title.isEmpty() ? title : "N/A");
    }

    private String getBookDescription(Document doc) {
        doc = doc.clone();
        doc.body().select("div#main-content.main-content > div.text_head").remove();
        doc.body().select("div#main-content.main-content > table").remove();

        String bookDesc = Jsoup.clean(
                doc.body().select("div#main-content.main-content").html(), Whitelist.none().addTags("br"));
        bookDesc = bookDesc
                .replaceAll("((<br>){2,})|((\n<br> )++)|((\n<br>)++)", "\n")
                .replaceAll("<br>", "")
                .trim();
        return bookDesc;
    }

    private Map<String, List<BookFullFile>> getBookFiles(Document doc) {
        Map<String, List<BookFullFile>> files = new HashMap<>();
        List<BookFullFile> list = new ArrayList<>();
        String title = "";

        Elements elements = doc.body().select("div#main-content.main-content > table > tbody > tr");
        for (Element element : elements) {
            String tTitle = element.select("td > div.text_head_small").text();
            if (!tTitle.isEmpty()) {
                if (!list.isEmpty()) {
                    if (title.isEmpty()) {
                        title = "N/A";
                    }
                    files.put(title, list);
                    list = new ArrayList<>();
                }
                title = tTitle;
                continue;
            }

            BookFullFile bookFile = new BookFullFile();
            bookFile.setTitle(getBookFileTitle(element));
            bookFile.setSubTitle(getBookFileSubTitle(element));
            bookFile.setLink(getBookFileLink(element));
            list.add(bookFile);
        }

        if (!list.isEmpty()) {
            if (title.isEmpty()) {
                title = "N/A";
            }
            files.put(title, list);
        }

        return files;
    }

    private String getBookFileTitle(Element element) {
        String title = element.select("td").get(1).text().trim();
        return (!title.isEmpty() ? title : "N/A");
    }

    private String getBookFileSubTitle(Element element) {
        String subTitle = element.select("td").get(0).text().trim();
        return (!subTitle.isEmpty() ? subTitle : "N/A");
    }

    private String getBookFileLink(Element element) {
        String link = element.select("td").get(1).select("td > a").attr("href").trim();
        return (!link.isEmpty() ? Constants.LIB_URL + link : "N/A");
    }
}
