package com.steiner.chdu.lib.parser.entity;

/**
 * Created by: Ihor Kushnirenko <SteinerOk@gmail.com>
 * Date: 20.12.2015 18:30
 */

public class ScBookShort {
    private String title;
    private String link;

    public ScBookShort() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

}
