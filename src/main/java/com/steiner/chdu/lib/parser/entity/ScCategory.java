package com.steiner.chdu.lib.parser.entity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by: Ihor Kushnirenko <SteinerOk@gmail.com>
 * Date: 20.12.2015 16:11
 */

public class ScCategory {
    private String name;
    private String subName;
    private String link;
    private Map<String, List<ScBookShort>> catalog;

    public ScCategory() {
        this.catalog = new HashMap<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubName() {
        return subName;
    }

    public void setSubName(String subName) {
        this.subName = subName;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Map<String, List<ScBookShort>> getCatalog() {
        return catalog;
    }

    public void setCatalog(Map<String, List<ScBookShort>> catalog) {
        this.catalog = catalog;
    }
}
