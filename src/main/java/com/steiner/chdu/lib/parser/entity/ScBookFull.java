package com.steiner.chdu.lib.parser.entity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by: Ihor Kushnirenko <SteinerOk@gmail.com>
 * Date: 20.12.2015 15:52
 */

public class ScBookFull {
    private String title;
    private String category;
    private String subCategory;
    private String link;
    private Map<String, List<ScArticle>> articlesList;

    public ScBookFull() {
        articlesList = new HashMap<>();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Map<String, List<ScArticle>> getArticlesList() {
        return articlesList;
    }

    public void setArticlesList(Map<String, List<ScArticle>> articlesList) {
        this.articlesList = articlesList;
    }

}
