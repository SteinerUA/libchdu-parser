package com.steiner.chdu.lib.parser.utils;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

/**
 * Created by: Ihor Kushnirenko <SteinerOk@gmail.com>
 * Date: 20.12.2015 12:23
 */

public class HttpUtils {

    public static String makeGetRequest(String url, OkHttpClient client) {
        if (null == client || null == url || url.isEmpty())
            return "";

        Request request = new Request.Builder()
                .url(url)
                .build();

        Response response;
        try {
            response = client.newCall(request).execute();
            return response.body().string();
        } catch (IOException e) {
            return "";
        }
    }
}
