package com.steiner.chdu.lib.parser.utils;

import com.steiner.chdu.lib.parser.enums.Categories;
import com.steiner.chdu.lib.parser.enums.ScCategories;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by: Ihor Kushnirenko <SteinerOk@gmail.com>
 * Date: 23.12.2015 20:57
 */

public class LibUtils {

    /**
     * Метод проверки существования категории с указаным id
     *
     * @param possId предполагаемый id категории
     * @return "true" если категориия существует, иначе "false"
     */
    public static boolean isIdInCategory(final int possId) {
        if (possId < 0)
            return false;

        final Categories[] categories = Categories.values();
        for (@NotNull Categories category : categories) {
            if (category.getId() == possId)
                return true;
        }
        return false;
    }

    /**
     * Метод проверки существования категории с указаным id в разделе "Научные публикации"
     *
     * @param possId предполагаемый id категории
     * @return "true" если категориия существует, иначе "false"
     */
    public static boolean isIdInScCategory(final int possId) {
        if (possId < 0)
            return false;

        final ScCategories[] categories = ScCategories.values();
        for (@NotNull ScCategories category : categories) {
            if (category.getId() == possId)
                return true;
        }
        return false;
    }

    /**
     * Метод проверки является ли url адресом категории
     *
     * @param url url-адрес
     * @return "true" если является адресом катеории, иначе "false"
     */
    public static boolean isCategoryUrl(@NotNull String url) {
        if (url.isEmpty())
            return false;

        @NotNull Pattern p = Pattern.compile("^http://lib\\.chdu\\.edu\\.ua/index\\.php\\?m=(\\d{1,2})$");
        @NotNull Matcher m = p.matcher(url);

        return m.matches() && LibUtils.isIdInCategory(Integer.valueOf(m.group(1)));
    }

    /**
     * Метод проверки является ли url адресом категории раздела "Научные публикации"
     *
     * @param url url-адрес
     * @return "true" если является адресом катеории из раздела "Научные публикации", иначе "false"
     */
    public static boolean isScCategoryUrl(@NotNull String url) {
        if (url.isEmpty())
            return false;

        @NotNull Pattern p = Pattern.compile("^http://lib\\.chdu\\.edu\\.ua/index\\.php\\?m=10&s=(\\d{1,2})$");
        @NotNull Matcher m = p.matcher(url);

        return m.matches() && LibUtils.isIdInScCategory(Integer.valueOf(m.group(1)));
    }

    /**
     * Метод проверки является ли url адресом книги из обычного раздела
     *
     * @param url url-адрес
     * @return "true" если является адресом книги, иначе "false"
     */
    public static boolean isBookUrl(@NotNull String url) {
        if (url.isEmpty())
            return false;

        @NotNull Pattern p = Pattern.compile("^http://lib\\.chdu\\.edu\\.ua/index\\.php\\?m=(\\d{1,2})&b=(\\d+)$");
        @NotNull Matcher m = p.matcher(url);

        return m.matches() && LibUtils.isIdInCategory(Integer.valueOf(m.group(1)));
    }

    /**
     * Метод проверки является ли url адресом книги из категории раздела "Научные публикации"
     *
     * @param url url-адрес
     * @return "true" если является адресом книги, иначе "false"
     */
    public static boolean isScBookUrl(@NotNull String url) {
        if (url.isEmpty())
            return false;

        @NotNull Pattern p = Pattern.compile("^http://lib\\.chdu\\.edu\\.ua/index\\.php\\?m=10&s=(\\d{1,2})&t=\\d+$");
        @NotNull Matcher m = p.matcher(url);

        return m.matches() && LibUtils.isIdInScCategory(Integer.valueOf(m.group(1)));
    }

    /**
     * Метод проверки существования и получения категории с указаным id
     *
     * @param possId предполагаемый id категории
     * @return категорию {@link Categories} с указаным id если она существует, иначе "null"
     */
    @Nullable
    public static Categories getCategoryById(final int possId) {
        if (possId < 0)
            return null;

        final Categories[] categories = Categories.values();
        for (@NotNull Categories category : categories) {
            if (category.getId() == possId)
                return category;
        }
        return null;
    }

    /**
     * Метод проверки существования и получения категории с указаным id в разделе "Научные публикации"
     *
     * @param possId предполагаемый id категории
     * @return категорию {@link ScCategories} с указаным id если она существует, иначе "null"
     */
    @Nullable
    public static ScCategories getScCategoryById(final int possId) {
        if (possId < 0)
            return null;

        final ScCategories[] categories = ScCategories.values();
        for (@NotNull ScCategories category : categories) {
            if (category.getId() == possId)
                return category;
        }
        return null;
    }

    /**
     * Метод проверки существования и получения категории с указаным url
     *
     * @param url url категории
     * @return категорию {@link Categories} для указаного url если она существует, иначе "null"
     */
    @Nullable
    public static Categories getCategoryByUrl(@NotNull String url) {
        if (url.isEmpty())
            return null;

        @NotNull Pattern p = Pattern.compile("^http://lib\\.chdu\\.edu\\.ua/index\\.php\\?m=(\\d{1,2})$");
        @NotNull Matcher m = p.matcher(url);
        if (!m.matches())
            return null;

        return LibUtils.getCategoryById(Integer.valueOf(m.group(1)));
    }

    /**
     * Метод проверки существования и получения категории с указаным url в разделе "Научные публикации"
     *
     * @param url url категории
     * @return категорию {@link Categories} для указаного url если она существует, иначе "null"
     */
    @Nullable
    public static ScCategories getScCategoryByUrl(@NotNull String url) {
        if (url.isEmpty())
            return null;

        @NotNull Pattern p = Pattern.compile("^http://lib\\.chdu\\.edu\\.ua/index\\.php\\?m=10&s=(\\d{1,2})$");
        @NotNull Matcher m = p.matcher(url);
        if (!m.matches())
            return null;

        return LibUtils.getScCategoryById(Integer.valueOf(m.group(1)));
    }

}
