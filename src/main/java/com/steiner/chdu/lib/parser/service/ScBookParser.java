package com.steiner.chdu.lib.parser.service;

import com.steiner.chdu.lib.parser.entity.ScArticle;
import com.steiner.chdu.lib.parser.entity.ScBookFull;
import com.steiner.chdu.lib.parser.utils.Constants;
import com.steiner.chdu.lib.parser.utils.HttpUtils;
import okhttp3.OkHttpClient;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by: Ihor Kushnirenko <SteinerOk@gmail.com>
 * Date: 20.12.2015 1:39
 */

public class ScBookParser {
    private String url;
    private OkHttpClient httpClient;

    public ScBookParser(String url) {
        this.url = url;
        this.httpClient = new OkHttpClient();
    }

    public ScBookParser(String url, OkHttpClient httpClient) {
        this.url = url;
        this.httpClient = httpClient;
    }

    public ScBookFull parse() {
        String response = HttpUtils.makeGetRequest(url, httpClient);
        if (response.isEmpty())
            return null;

        Document document = Jsoup.parse(response);

        ScBookFull book = new ScBookFull();
        book.setLink(url);
        book.setCategory(getScBookCategory(document));
        book.setSubCategory(getScBookSubCategory(document));
        book.setTitle(getScBookTitle(document));
        book.setArticlesList(getScArticlesSection(document));
        return book;
    }

    private String getScBookCategory(Document doc) {
        String category = doc.body().select("div#main-content.main-content > div.text_head > a")
                .get(0).text().trim();
        return (!category.isEmpty() ? category : "N/A");
    }

    private String getScBookSubCategory(Document doc) {
        String subCategory = doc.body().select("div#main-content.main-content > div.text_head > a")
                .get(1).text().trim();
        return (!subCategory.isEmpty() ? subCategory : "N/A");
    }

    private String getScBookTitle(Document doc) {
        doc = doc.clone();
        doc.body().select("div#main-content.main-content > div.text_head > a").remove();
        String title = doc.body().select("div#main-content.main-content > div.text_head")
                .text().replace("- ", "").trim();
        return (!title.isEmpty() ? title : "N/A");
    }

    private Map<String, List<ScArticle>> getScArticlesSection(Document doc) {
        Map<String, List<ScArticle>> articlesList = new HashMap<>();
        String articlesSectionTitle = "";
        List<ScArticle> articles = new ArrayList<>();
        ScArticle article = new ScArticle();

        Elements contentTable = doc.body().select("div#main-content.main-content > table > tbody > tr > td");
        for (Element element : contentTable) {
            if (!element.hasText())
                continue;   //Пропускаем пустые блоки <td>

            Elements tElements;
            tElements = element.select("div.text_head_small");  //Заголовок цикла статтей
            if (tElements.hasText()) {
                if (!articlesSectionTitle.isEmpty() && !articles.isEmpty()) {
                    articles.add(article);
                    article = new ScArticle();
                    articlesList.put(articlesSectionTitle, articles);
                    articles = new ArrayList<>();
                }
                articlesSectionTitle = tElements.text();
                continue;
            }

            tElements = element.select("b");    //Статьи
            if (tElements.hasText()) {
                if (tElements.select("a").hasText()) {
                    if (null != article.getTitle()) {
                        articles.add(article);
                        article = new ScArticle();
                    }

                    article.setTitle(getScArticleTitle(tElements));
                    article.setAuthors(getScArticleAuthors(tElements, article.getTitle()));
                    article.setLink(getScArticleLink(tElements));
                }
            } else if (element.text().startsWith("Ключові слова:")) {
                article.setKeywords(getScArticleKeywords(element));
            } else {
                article.setDescriptions(getScArticleDesc(element));
            }
        }

        if (null != article.getTitle()) {
            articles.add(article);
        }
        if (!articlesSectionTitle.isEmpty() && !articles.isEmpty()) {
            articlesList.put(articlesSectionTitle, articles);
        }
        return articlesList;
    }

    private String getScArticleTitle(Elements elements) {
        String title = elements.select("a").text().trim();
        return (!title.isEmpty() ? title : "N/A");
    }

    private String getScArticleLink(Elements elements) {
        String link = elements.select("a").attr("href").trim();
        return (!link.isEmpty() ? Constants.LIB_URL + link : "N/A");
    }

    private String[] getScArticleAuthors(Elements elements, String title) {
        String s = elements.text().substring(0, elements.text().indexOf(title)).trim();
        if (s.isEmpty())
            return new String[0];

        String[] authors = s.split(",");
        for (int i = 0; i < authors.length; i++) {
            authors[i] = authors[i].trim();
        }
        return authors;
    }

    private String[] getScArticleKeywords(Element element) {
        if (!element.hasText())
            return new String[0];

        String s = element.text().replaceAll("(Ключові слова:)|(\\.)", "");
        String[] keywords = s.split(",");
        for (int i = 0; i < keywords.length; i++) {
            keywords[i] = keywords[i].trim();
        }

        return keywords;
    }

    private String[] getScArticleDesc(Element element) {
        String s = element.html();
        if (s.isEmpty())
            return new String[0];

        String[] descs = s.split("<br><br>");
        for (int i = 0; i < descs.length; i++) {
            descs[i] = descs[i].replaceAll("<br>", " ").trim();
        }
        return descs;
    }

}
