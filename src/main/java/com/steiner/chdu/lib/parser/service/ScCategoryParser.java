package com.steiner.chdu.lib.parser.service;

import com.steiner.chdu.lib.parser.entity.ScBookShort;
import com.steiner.chdu.lib.parser.entity.ScCategory;
import com.steiner.chdu.lib.parser.enums.ScCategories;
import com.steiner.chdu.lib.parser.utils.Constants;
import com.steiner.chdu.lib.parser.utils.HttpUtils;
import okhttp3.OkHttpClient;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by: Ihor Kushnirenko <SteinerOk@gmail.com>
 * Date: 20.12.2015 16:08
 */

public class ScCategoryParser {
    private ScCategories selectedScCategory;
    private OkHttpClient httpClient;

    public ScCategoryParser(ScCategories selectedScCategory) {
        this.selectedScCategory = selectedScCategory;
        this.httpClient = new OkHttpClient();
    }

    public ScCategoryParser(ScCategories selectedScCategory, OkHttpClient httpClient) {
        this.selectedScCategory = selectedScCategory;
        this.httpClient = httpClient;
    }

    public ScCategory parse() {
        String url = Constants.LIB_SC_SUB_CAT_URL + selectedScCategory.getId();
        String response = HttpUtils.makeGetRequest(url, httpClient);
        if (response.isEmpty())
            return null;

        Document document = Jsoup.parse(response);

        ScCategory scCategory = new ScCategory();
        scCategory.setLink(url);
        scCategory.setName(getScCatName(document));
        scCategory.setSubName(getScCatSubName(document));
        scCategory.setCatalog(getScCatCatalog(document));

        return scCategory;
    }

    private String getScCatName(Document doc) {
        String name = doc.body().select("div#main-content.main-content > div.text_head > a").text().trim();
        return (!name.isEmpty() ? name : "N/A");
    }

    private String getScCatSubName(Document doc) {
        String subName = doc.body().select("div#main-content.main-content > div.text_head").text()
                .replaceFirst("-", "")
                .trim();
        return (!subName.isEmpty() ? subName : "N/A");
    }

    private Map<String, List<ScBookShort>> getScCatCatalog(Document doc) {
        Map<String, List<ScBookShort>> catalog = new HashMap<>();
        List<ScBookShort> booksList = new ArrayList<>();
        String year = "";

        Elements elements = doc.select("div#main-content.main-content > table > tbody > tr > td");
        for (Element element : elements) {
            if (!element.hasText() && element.isBlock())
                continue;   //Пропускаем пустые блоки <td>

            String tYear = element.select("td > div.text_head_small").text().trim(); //Заголовок (год) книг
            if (4 == tYear.length()) {
                if (!year.isEmpty() && !booksList.isEmpty()) {
                    catalog.put(year, booksList);
                    booksList = new ArrayList<>();
                }
                year = tYear;
            } else {
                booksList.add(getScCatCatalogBook(element));
            }
        }

        if (!year.isEmpty() && !booksList.isEmpty()) {
            catalog.put(year, booksList);
        }
        return catalog;
    }

    private ScBookShort getScCatCatalogBook(Element element) {
        ScBookShort book = new ScBookShort();

        String tValue = element.text().trim();
        book.setTitle(!tValue.isEmpty() ? tValue : "N/A");

        tValue = element.select("td > a[href]").attr("href").trim();
        book.setLink(!tValue.isEmpty() ? Constants.LIB_URL + tValue : "N/A");
        return book;
    }
}
