package com.steiner.chdu.lib.parser.utils;

/**
 * Created by: Ihor Kushnirenko <SteinerOk@gmail.com>
 * Date: 19.12.2015 16:53
 */

public final class Constants {
    /**
     * Базовый адрес сайта библиотеки
     */
    public final static String LIB_URL = "http://lib.chdu.edu.ua/";

    /**
     * Основа для адреса страници категории.
     * Для получения полной ссылки присоеденить одно из значений списка
     * {@link com.steiner.chdu.lib.parser.enums.Categories}
     */
    public final static String LIB_CAT_URL = LIB_URL + "index.php?m=";

    /**
     * Суфикс для ссылки на категорию из раздела "Наукові праці"
     */
    public final static String LIB_CAT_SC_SUF = "10&s=";

    /**
     * Основа для адреса страници категории из раздела "Наукові праці".
     * Для получения полной ссылки присоеденить одно из значений списка
     * {@link com.steiner.chdu.lib.parser.enums.ScCategories}
     */
    public final static String LIB_SC_SUB_CAT_URL = LIB_CAT_URL + LIB_CAT_SC_SUF;
}
